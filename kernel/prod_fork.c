#include <unistd.h>
#include <stdlib.h> 
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>

int main(int argc, char *argv[]){
  pid_t pid1, pid2;
  char *argp1[] = {"prod", NULL ,NULL};
  int status1, status2;
  pid1 = fork();

  if(pid1 < 0){
    printf("impossible de créer le processus\n");
    exit(-1);
  }
  
  if(pid1 == 0){
       execv("./prod", argp1);
  }

  else{
     pid2 = fork();
     if(pid2 < 0){
         printf("impossible de créer le processus\n");
         exit(-1);
     }
  
     if(pid2 == 0){
         execv("./prod", argp1);
     }

     else{
       pid1 = wait(&status1);
       pid2 = wait(&status2);
       exit(0);
    }
 
  }

}
