#include <unistd.h>
#include <stdlib.h> 
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#define NPROCS 20

int main(int argc, char *argv[]){
  pid_t pid[NPROCS];
  char *argp1[] = {"prod", NULL ,NULL};
  int status[NPROCS];
  for(int i = 0; i < NPROCS; i++){
     pid[i] = fork();

     if(pid[i] < 0){
        printf("impossible de créer le processus\n");
        exit(-1);
    } 
    if(pid[i] == 0){
       printf("le fils %d Création du processus fils %d %d\n", i , 
                 getpid(), getppid());
       execv("./prod", argp1);
    }
    else printf("Création du processus fils %d\n", getpid()); 
  }
  for(int i = 0; i < NPROCS; i++){
       pid[i] = wait(&status[i]);
    }
    exit(0);

}
