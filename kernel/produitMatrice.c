#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#define DIM 1000
 long matrice[DIM][DIM];
 long produit[DIM][DIM];

int main(int argc, char *argv[]){
  for(int i = 0; i < DIM; i++)
    for(int j = 0; j < DIM; j++)
        matrice[i][j] = getpid();

 for(int i = 0; i < DIM; i++)
    for(int j = 0; j < DIM; j++){
        produit[i][j] = 0;
        for(int k = 0; k < DIM; k++)
           produit[i][j]+= matrice[i][k] * matrice[k][j];
    }
 
 for(int i = 0; i < DIM; i++){
    printf("\n");
    for(int j = 0; j < DIM; j++)
        printf("%ld\t", produit[i][j]);
 }
}
