#include "kernel/kernel.h"
#include "kernel/vm.h"

#include <machine/vm.h>

#include <minix/type.h>
#include <minix/syslib.h>
#include <minix/cpufeature.h>
#include <string.h>
#include <assert.h>
#include <signal.h>
#include <stdlib.h>


#include <machine/vm.h>

#include "oxpcie.h"
#include "arch_proto.h"
//#include "proto.h"
#include "rcounter.h"

#ifdef USE_APIC
#include "apic.h"
#ifdef USE_WATCHDOG
#include "kernel/watchdog.h"
#endif
#endif

static void vm_print(u32_t *root);
static void vm_pt_print(u32_t *pagetable, const u32_t v);
static char *flagstr(u32_t e, const int dir);
static char *cr0_str(u32_t e);
static void display_mem(struct proc *current_p);
static void display_mem_pram_block(struct proc *current_p, vir_bytes vaddr);
static struct pram_mem_block *get_pmb(void);
void free_pram_mem_blocks(struct proc *current_p);
void free_pram_mem_block(struct proc *current_p, vir_bytes v_addr);
static void vm_setpt_to_ro(struct proc *p, u32_t *pagetable, const u32_t v, int rw );
//static void free_pram_mem_blocks(struct proc *current_p);

static int dwc_enable = 0;

static int count_set_ro = 0;

/*This function is used to update all the process pages as RO
 * add at 30/12/2014 */
static void vm_setpt_to_ro(struct proc *p, u32_t *pagetable, const u32_t v, int rw)
{
	int pte, pfa;
	assert(!((u32_t) pagetable % I386_PAGE_SIZE));

	for(pte = 0; pte < I386_VM_PT_ENTRIES; pte++) {
		u32_t pte_v, pfa ;
		pte_v = phys_get32((u32_t) (pagetable + pte));

		if(!(pte_v & I386_VM_PRESENT))
			continue;
                if(!(pte_v & I386_VM_USER))
			continue;
                
		pfa = I386_VM_PFA(pte_v);
                if(pfa){
#if 1
                   struct pram_mem_block *pmb, *next_pmb;
                   pmb = get_pmb();
                   assert(pmb);
                   pmb->pram_phys =  pfa;
                   pmb->vaddr     =  v + pte * I386_PAGE_SIZE;
                   if(!p->p_first_step_workingset_id)
                      p->p_working_set = pmb;
                   else{
                         next_pmb = p->p_working_set;
                         while(next_pmb->next_pmb)
                         next_pmb = next_pmb->next_pmb; 
                         next_pmb->next_pmb = pmb;
                         next_pmb->next_pmb->next_pmb = NULL;
                   }
                   pmb->id = p->p_first_step_workingset_id;
                   p->p_first_step_workingset_id++;
                   
                   pte_v &= ~I386_VM_USER;
                   phys_set32((u32_t) (pagetable + pte), &pte_v);
                   //h_procs_in_use++;
#endif
                 }
	}
       // printf("set Number of blocks %d %d\n", h_procs_in_use, p->p_nr);
        //h_procs_in_use = 0;
	return;
}

void vm_reset_pt(struct proc *p, u32_t *root){
   assert(p->p_nr != VM_PROC_NR);
   if(p->p_first_step_workingset_id > 0){
      struct pram_mem_block *pmb = p->p_working_set;
      while(pmb){
         int pde = I386_VM_PDE(pmb->vaddr), pte = I386_VM_PTE(pmb->vaddr);
         u32_t pde_v, *pte_a, pte_v;
         pde_v = phys_get32((u32_t) (root + pde));
         if(!(pde_v & I386_VM_PRESENT)){
             pmb = pmb->next_pmb;
	     continue;
         }
         if(!(pde_v & I386_VM_WRITE)){
             pmb = pmb->next_pmb;
	     continue;
         }
         if(!(pde_v & I386_VM_USER)){
             pmb = pmb->next_pmb;
	     continue;
         }
         if((pde_v & I386_VM_GLOBAL)){
             pmb = pmb->next_pmb;
	     continue;
         }
         if((pde_v & I386_VM_BIGPAGE)){
             pmb = pmb->next_pmb;
	     continue;
         }
         pte_a = (u32_t *) I386_VM_PFA(pde_v);
         pte_v = phys_get32((u32_t) (pte_a + pte));
         if(!(pte_v & I386_VM_PRESENT)){
	     pmb = pmb->next_pmb;
	     continue;
         }
          if((pte_v & I386_VM_USER)){
             pmb = pmb->next_pmb;
	     continue;
         }
         u32_t pfa = I386_VM_PFA(pte_v);
         assert(pfa);
         assert(pfa == pmb->pram_phys);
         pte_v |= I386_VM_USER;
         phys_set32((u32_t) (pte_a + pte), &pte_v);
         h_procs_in_use++;
         pmb = pmb->next_pmb;
      }
   //printf("reset Number of blocks %d %d\n", h_procs_in_use, p->p_nr);
   //h_procs_in_use = 0;
  }
}
void vm_setpt_root_to_ro(struct proc *p, u32_t *root, int rw){
#if 0
      vm_print(root);
#endif
	int pde;
	assert(!((u32_t) root % I386_PAGE_SIZE));
        assert(p->p_nr != VM_PROC_NR);
	for(pde = 0; pde < I386_VM_DIR_ENTRIES; pde++) {
		u32_t pde_v;
		u32_t *pte_a;
		pde_v = phys_get32((u32_t) (root + pde));
		if(!(pde_v & I386_VM_PRESENT))
			continue;
                if(!(pde_v & I386_VM_WRITE))
			continue;
                if(!(pde_v & I386_VM_USER))
			continue;
                if((pde_v & I386_VM_GLOBAL))
			continue;
                if((pde_v & I386_VM_BIGPAGE))
			continue;
		pte_a = (u32_t *) I386_VM_PFA(pde_v);
		if(pde_v & I386_VM_USER){
			vm_setpt_to_ro(p, pte_a, pde * I386_VM_PT_ENTRIES * I386_PAGE_SIZE, rw);


		}
	}

	return;
}

int check_vaddr(struct proc *p, u32_t *root, vir_bytes vaddr){
   int pde = I386_VM_PDE(vaddr), pte = I386_VM_PTE(vaddr);
   u32_t pde_v, *pte_a, pte_v, pte_v2;
   pde_v = phys_get32((u32_t) (root + pde));
   //printf("pdev 0x%lx 0x%lx \n", vaddr, pde_v);
   if(!(pde_v & I386_VM_PRESENT))
	return(-1);
   if(!(pde_v & I386_VM_WRITE))
	return(-1);
   if(!(pde_v & I386_VM_USER))
	return(-1);
   if((pde_v & I386_VM_GLOBAL))
	return(-1);
   if((pde_v & I386_VM_BIGPAGE))
	return(-1);
   pte_a = (u32_t *) I386_VM_PFA(pde_v);
   pte_v = phys_get32((u32_t) (pte_a + pte));
   //printf("pte_v 0x%lx 0x%lx \n", vaddr, pte_v);
   if(!(pte_v & I386_VM_PRESENT))
	return(-1);
    if((pte_v & I386_VM_USER))
	return(-1);
   /*if((pte_v & I386_VM_BIGPAGE))
	return(-1);*/
   //printf("after ptev 0x%lx 0x%lx \n", vaddr, pte_v);
   struct pram_mem_block * pmb = look_up_pte(p, I386_VM_PDE(vaddr),
                               I386_VM_PTE(vaddr) );
   u32_t pfa = I386_VM_PFA(pte_v);

   assert(pmb);
   assert(pfa == pmb->pram_phys);
   pte_v |=  I386_VM_USER;
   phys_set32((u32_t) (pte_a + pte), &pte_v);
   h_procs_in_use++;
   //printf("Page fault 0x%lx 0x%lx\n", vaddr, pfa);
#if LOCALITY___
   for(int i = 1; i <= 5 && (pte + i) <I386_VM_PT_ENTRIES; i++){
       pte_v = phys_get32((u32_t) (pte_a + pte + i));
       if(!(pte_v & I386_VM_PRESENT))
	 continue;
       if(!(pte_v & I386_VM_USER) && (pte_v & I386_VM_PTAVAIL2)){
           pte_v |=  I386_VM_USER;
           pte_v &= ~I386_VM_PTAVAIL2;
           printf("set deeper PF en un nom user 0x%lx\n", vaddr);
           phys_set32((u32_t) (pte_a + pte), &pte_v);
       }
    }
#endif
  return(OK);
  //if(!(pte_v & I386_VM_USER) /*&& (pte_v & I386_VM_PTAVAIL3)*/){
    //  printf("hahah PF en un nom user 0x%lx\n", vaddr);
  // }
}


int toogle_pt_in_kernel(struct proc *rp, u32_t *root, vir_bytes vaddr,  int rw, vir_bytes bytes){
	u32_t pde_v;
	u32_t *pte_a;
	int npt;
	int pde, pte_i,pte;
	return(OK);
	if(!dwc_enable)
		return OK;
	/*printf("Start toogle \n");
	printf("Start toogle %d\n", rp->p_endpoint);*/
	if(rp->p_endpoint == VM_PROC_NR || rp->p_endpoint < PM_PROC_NR ||
			(rp->p_hflags & H_FORK_IN_PROGRESS) /*||
			rp->p_endpoint == AT_WINI_PROC_NR */||
			!(rp->p_hflags & H_ENABLE)) {
		//		printf("Error pas pour le VM %d\n", rp->p_endpoint);
		return OK;
	}

	/*if(!isokendpt(rp->p_endpoint, &rp)){
		printf("Bad endpt stop\n");
		return EFAULT;
	}*/
	//	display_mem(rp);
	assert(!((u32_t) root % I386_PAGE_SIZE));
	pde = I386_VM_PDE(vaddr); pte_i = I386_VM_PTE(vaddr);
	pde_v = phys_get32((u32_t) ((u32_t)(root + pde)));
	npt = (
			((vaddr + bytes)  -
					(pde * I386_VM_PT_ENTRIES * I386_PAGE_SIZE + pte_i * I386_PAGE_SIZE))
					/ I386_PAGE_SIZE) + 1;

	if(!(pde_v & I386_VM_PRESENT)){
		//printf("pde not present %d\n", pde);
		return EFAULT;
	}
	pte_a = (u32_t *) I386_VM_PFA(pde_v);
	if(pde_v & I386_VM_USER){
		assert(!((u32_t) pte_a % I386_PAGE_SIZE));
		u32_t pte_v, pfa;
		for(pte = pte_i; pte < pte_i + npt; pte++){
			pte_v = phys_get32((u32_t) (pte_a + pte));
			if(!(pte_v & I386_VM_PRESENT)){
				//printf("pte not present %d\n", pte);
				continue;
			}

			if(!(pte_v & I386_VM_USER)){
				//printf("pte not USER %d\n", pte);
				continue;
			}
			if(rw == I386_VM_READ ){
				pte_v &= ~I386_VM_WRITE;
				pte_v |=  I386_VM_READ;
			}
			else {
				pte_v &= ~I386_VM_READ;
				pte_v |=  I386_VM_WRITE;
			}
			phys_set32((u32_t) (pte_a + pte), &pte_v);
		}
	}
	//	printf("end toogle %d\n", rp->p_endpoint);
	return(OK);
}

static char *cr0_str(u32_t e)
{
	static char str[80];
	strcpy(str, "");
#define FLAG(v) do { if(e & (v)) { strcat(str, #v " "); e &= ~v; } } while(0)
	FLAG(I386_CR0_PE);
	FLAG(I386_CR0_MP);
	FLAG(I386_CR0_EM);
	FLAG(I386_CR0_TS);
	FLAG(I386_CR0_ET);
	FLAG(I386_CR0_PG);
	FLAG(I386_CR0_WP);
	if(e) { strcat(str, " (++)"); }
	return str;
}

static char *flagstr(u32_t e, const int dir)
{
	static char str[80];
	strcpy(str, "");
	FLAG(I386_VM_PRESENT);
	FLAG(I386_VM_WRITE);
	FLAG(I386_VM_USER);
	FLAG(I386_VM_PWT);
	FLAG(I386_VM_PCD);
	FLAG(I386_VM_GLOBAL);
	if(dir)
		FLAG(I386_VM_BIGPAGE);	/* Page directory entry only */
	else
		FLAG(I386_VM_DIRTY);	/* Page table entry only */
	return str;
}

static void vm_pt_print(u32_t *pagetable, const u32_t v)
{
	int pte;
	int col = 0;

	assert(!((u32_t) pagetable % I386_PAGE_SIZE));

	for(pte = 0; pte < I386_VM_PT_ENTRIES; pte++) {
		u32_t pte_v, pfa;
		pte_v = phys_get32((u32_t) (pagetable + pte));
		if(!(pte_v & I386_VM_PRESENT))
			continue;
		pfa = I386_VM_PFA(pte_v);
		printf("%4d:%08lx:%08lx %2s %s ",
				pte, v + I386_PAGE_SIZE*pte, pfa,
				(pte_v & I386_VM_WRITE) ? "rw":"RO", flagstr(pte_v, 1));
		col++;
		if(col == 3) { printf("\n"); col = 0; }
	}
	if(col > 0) printf("\n");

	return;
}
static void vm_print(u32_t *root)
{
	int pde;

	assert(!((u32_t) root % I386_PAGE_SIZE));

	printf("page table 0x%lx:\n", root);

	for(pde = 0; pde < I386_VM_DIR_ENTRIES; pde++) {
		u32_t pde_v;
		u32_t *pte_a;
		pde_v = phys_get32((u32_t) (root + pde));
		if(!(pde_v & I386_VM_PRESENT))
			continue;
		if(pde_v & I386_VM_BIGPAGE) {
			printf("%4d: 0x%lx, flags %s\n",
					pde, I386_VM_PFA(pde_v), flagstr(pde_v, 1));
		} else {
			pte_a = (u32_t *) I386_VM_PFA(pde_v);
			printf("%4d: pt %08lx %s\n",
					pde, pte_a, flagstr(pde_v, 1));
			vm_pt_print(pte_a, pde * I386_VM_PT_ENTRIES * I386_PAGE_SIZE);
			printf("\n");
		}
	}


	return;
}

/*retourne le premier au cas où tous est oqp, cela devient alors cyclique
 * On espere aloer ce qu'il faut d'élemetn pour le system*/
static struct pram_mem_block *get_pmb(void){
	struct pram_mem_block *pmb = BEG_PRAM_MEM_BLOCK_ADDR;
	int count = 0;
	if(pmb->flags & PRAM_SLOT_FREE) {
		pmb->flags &=~PRAM_SLOT_FREE;
		pmb->current_crc32 = 0;
		return pmb;
	}
	do{
		count++;
		pmb++;
	}while(!(pmb->flags & PRAM_SLOT_FREE) && pmb < END_PRAM_MEM_BLOCK_ADDR);
	pmb->flags &=~PRAM_SLOT_FREE;
	pmb->current_crc32 = 0;
	if(count>= WORKING_SET_SIZE-2)
		printf("out of pmb hihi %d\n", count);
	return pmb;
}

static void display_mem(struct proc *current_p){
	if(current_p->p_first_step_workingset_id > 0){
		struct pram_mem_block *pmb = current_p->p_working_set;
		while(pmb){
			printf("vaddr 0x%lx pram 0x%lx temp 0x%lx 0x%08x\n", pmb->vaddr,
					pmb->pram_phys, pmb->second_step_phys, pmb->current_crc32);
			pmb = pmb->next_pmb;
		}
	}
}

static void display_mem_pram_block(struct proc *current_p, vir_bytes vaddr){
	if(current_p->p_first_step_workingset_id > 0){
		struct pram_mem_block *pmb = current_p->p_working_set;
		while(pmb){
			if(pmb->vaddr == vaddr)
				printf("vaddr 0x%lx pram 0x%lx temp 0x%lx 0x%08x\n", pmb->vaddr,
						pmb->pram_phys, pmb->second_step_phys, pmb->current_crc32);
			pmb = pmb->next_pmb;
		}
	}
}

void free_pram_mem_blocks(struct proc *current_p){
	if(current_p->p_first_step_workingset_id > 0){
		struct pram_mem_block *pmb = current_p->p_working_set;
		while(pmb){
			//set_frame_pmb(current_p, current_p->p_seg.p_cr3, pmb);
			pmb->flags = PRAM_SLOT_FREE;
                        pmb->vaddr = 0;
                        pmb->id    = 0; 
                        pmb->pram_phys = 0;
			pmb = pmb->next_pmb;
		}
		current_p->p_first_step_workingset_id = 0;
		current_p->p_working_set = NULL;
	}
}

void free_pram_mem_block(struct proc *current_p, vir_bytes vaddr){
	if(current_p->p_first_step_workingset_id > 0){
		struct pram_mem_block *pmb = current_p->p_working_set, *prev_pmb = NULL, *next_pmb = NULL;
		while(pmb){

			if(pmb->vaddr == vaddr && prev_pmb){
				pmb->flags = PRAM_SLOT_FREE;
				prev_pmb->next_pmb = pmb->next_pmb;
				break;
			}
			prev_pmb = pmb;
			next_pmb = pmb->next_pmb;
			pmb = pmb->next_pmb;
		}
		current_p->p_first_step_workingset_id--;
		//current_p->p_working_set = NULL;
	}
}

int set_frame_pmb(struct proc *p, u32_t *root, struct pram_mem_block *pmb){
	int pde, pte;
	u32_t pde_v;
	u32_t *pte_a;
	u32_t pte_v, pfa;
	assert(!((u32_t) root % I386_PAGE_SIZE));
	pde = I386_VM_PDE(pmb->vaddr); pte = I386_VM_PTE(pmb->vaddr);
	pde_v = phys_get32((u32_t) (root + pde));
	if(!(pde_v & I386_VM_PRESENT)){
		free_pram_mem_block(p, pmb->vaddr);
		return(EFAULT);
	}
	pte_a = (u32_t *) I386_VM_PFA(pde_v);

	if(!(pde_v & I386_VM_WRITE)){
		printf("set_frame_pmb: pde_v not writable 0x%lx, 0x%lx %d\n",
				pmb->vaddr, pmb->pram_phys, p->p_endpoint);
		free_pram_mem_block(p, pmb->vaddr);
		return(EFAULT);
	}
	pte_v = phys_get32((u32_t) (pte_a + pte));

	if(!(pte_v & I386_VM_PRESENT)){
		printf("set_frame_pmb: not present 0x%lx, 0x%lx %d %d 0x%lx\n",
				pmb->vaddr, pmb->pram_phys, p->p_endpoint, pmb->flags, pfa);
		free_pram_mem_block(p, pmb->vaddr);
		return(EFAULT);
	}

	pfa = I386_VM_PFA(pte_v);

	if(pmb->pram_phys != pfa){
		printf("set_frame_pmb: pfa not ident 0x%lx, 0x%lx %d %d 0x%lx\n",
				pmb->vaddr, pmb->pram_phys, p->p_endpoint, pmb->flags, pfa);
		free_pram_mem_block(p, pmb->vaddr);
		return(EFAULT);
	}

	if((pte_v & I386_VM_USER)){
#if 0
		printf("set_frame_pmb: pte_v is USER 0x%lx, 0x%lx %d\n",
				pmb->vaddr, pmb->pram_phys, p->p_endpoint);
#endif
		pmb->flags |= PMB_IS_USER;
		return(OK);

	}
	else {
#if 0
		printf("set_frame_pmb: pte_v is NOT USER 0x%lx, 0x%lx %d\n",
				pmb->vaddr, pmb->pram_phys, p->p_endpoint);
#endif
	}

	pte_v |= I386_VM_USER;
	/*	pte_v &= ~I386_VM_READ;
	pte_v |= I386_VM_WRITE;*/

	phys_set32((u32_t) (pte_a + pte), &pte_v);
#if 0
	pte_v = phys_get32((u32_t) (pte_a + pte));

	if(!(pte_v & I386_VM_USER))
		printf("kernel: set_frame_pmb in pr %d, addr 0x%lx, his cr3 0x%lx, actual cr3 0x%lx\n",
				p->p_endpoint, pmb->vaddr, p->p_seg.p_cr3, read_cr3());
#endif
	return(OK);
}

u32_t compute_crc32(struct proc *p, struct proc *caller, vir_bytes v){
	u32_t crc32 = 0;
	switch_address_space(p);
	//crc32 = crc32c(crc32, v, I386_PAGE_SIZE);
	switch_address_space(caller);
	return crc32;
}
