#include "kernel/kernel.h"
#include "kernel/watchdog.h"
#include "arch_proto.h"
#include "glo.h"
#include <minix/minlib.h>
#include <minix/u64.h>
#include <assert.h>

#include "rcounter.h"
//#include "proto.h"

#if 0
void show_sys_context_switch(struct proc *p){
	printf("sysenter %d, syscall %d, int_hard %d, int_um %d, "
			"fullcontext %d, int_orig %d step %d from %d\n",
			p->p_sysenter, p->p_syscall, p->p_int_hard,
			p->p_int_um, p->p_fullcontext, p->p_int_orig,
			p->p_hardening_step, p->p_switch_from);
}

void proc_stopped(struct proc *p){

	switch(p->p_seg.p_kern_trap_style){
	case  KTS_SYSENTER:
	case  KTS_SYSCALL:
	case  KTS_INT_ORIG:
	case  KTS_INT_UM:
		if(p->p_hardening_step == HARDENING_FIRST_STEP)
			p->p_switch_from |= INT_BY_SYSCALL;
		else if(p->p_hardening_step == HARDENING_SECOND_STEP){
			p->p_switch_from &= ~INT_BY_SYSCALL;
			p->p_hardening_step = HARDENING_ZERO_STEP;
			if(p->p_endpoint != RS_PROC_NR)
			free_pram_mem_blocks(p);
		}
		if(p->p_hardening_step == HARDENING_FIRST_STEP ||
				p->p_hardening_step == HARDENING_SECOND_STEP)
			free_pram_mem_blocks(p);
		break;
	case  KTS_INT_HARD:
		p->p_int_hard++;
		break;

		p->p_int_um++;
		break;
	case  KTS_FULLCONTEXT:
		p->p_fullcontext++;
		break;
	default  :
		p->p_none++;
		break;

	}
}

void proc_restored(struct proc *p){
	if((p->p_nr != VM_PROC_NR) && p->p_endpoint >= 0 ){
#if USE_RO1
		vm_setpt_root_to_ro(p, p->p_seg.p_cr3, I386_VM_READ, p->p_nr);
#endif

#if USE_RO
		if(p->p_hardening_step == HARDENING_ZERO_STEP){
			p->p_hardening_step = HARDENING_FIRST_STEP;
			vm_setpt_root_to_ro(p, p->p_seg.p_cr3, I386_VM_READ, p->p_nr);
		}
		else if(p->p_hardening_step == HARDENING_FIRST_STEP &&
				(p->p_switch_from & INT_BY_SYSCALL)){
			p->p_hardening_step = HARDENING_SECOND_STEP;
			vm_setpt_root_to_ro(p, p->p_seg.p_cr3, I386_VM_READ, p->p_nr);
		}
#endif
	}
}
#endif
struct pram_mem_block * look_up_pte(struct proc *current_p, int pde, int pte){
	if(current_p->p_first_step_workingset_id > 0){
		struct pram_mem_block *pmb = current_p->p_working_set;
		while(pmb){
			if(pte==I386_VM_PTE(pmb->vaddr)&&pde==I386_VM_PDE(pmb->vaddr)){
				return(pmb);
			}
			pmb = pmb->next_pmb;
		}
	}
	return((struct pram_mem_block *)NULL);
}


struct pram_mem_block * look_up_ptes(struct proc *current_p, int pde, int pte){
	if(current_p->p_first_step_workingset_id > 0){
		struct pram_mem_block *pmb = current_p->p_working_set;
		struct pram_mem_block *c_pmb = (struct pram_mem_block *)NULL;
		int k = 0;
		while(pmb){
			if(pte==I386_VM_PTE(pmb->vaddr)&&pde==I386_VM_PDE(pmb->vaddr)){
				c_pmb = pmb;
				k = 1;
			/*	printf(" titivaddr 0x%lx pram 0x%lx temp 0x%lx 0x%08x\n", (pmb)->vaddr,
						(pmb)->pram_phys, (pmb)->second_step_phys, (pmb)->current_crc32);*/
			}
			pmb = pmb->next_pmb;
			if(c_pmb && pmb){
				if(pte + k ==I386_VM_PTE((pmb)->vaddr)&&pde==I386_VM_PDE((pmb)->vaddr)){
					/*printf("vaddr 0x%lx pram 0x%lx temp 0x%lx 0x%08x\n", (pmb)->vaddr,
							(pmb)->pram_phys, (pmb)->second_step_phys, (pmb)->current_crc32);*/
					//set_frame_pmb(current_p, current_p->p_seg.p_cr3, pmb);
				}
			}
			k++;
		}
		return(c_pmb);
	}
	return((struct pram_mem_block *)NULL);
}

void display_pmbs(struct pram_mem_block ** pmbs){

	struct pram_mem_block *pmb = (*pmbs);
	while(pmb){
		printf("vaddr 0x%lx pram 0x%lx temp 0x%lx 0x%08x\n", pmb->vaddr,
				pmb->pram_phys, pmb->second_step_phys, pmb->current_crc32);
		pmb = pmb->next_pmb;
	}

}

int prepare_4_second_exec(){
    /**/
    return(OK);
}

int comparison_ws(){
   /**/
   return(OK);
}

int prepare_4_first_exec(){
   /**/
   return(OK);
}


int reset_hard(struct proc *p){

/* this function is responsible to:
   - prepare hp for 2nd run
   - start the comparison
   - start the first run if the second is notre able. */

     /*code temporaire*/
     h_enable = 0;
     h_proc_nr = 0;
     p->p_start_count_ins = 0;
#if COW
     assert(p->p_nr != VM_PROC_NR);
     struct proc *hptproc = get_cpulocal_var_ptr(ptproc);
     //switch_address_space(p);
     vm_reset_pt(p,p->p_seg.p_cr3);
     free_pram_mem_blocks(p);
     //if(hptproc->p_nr != p->p_nr)
       // switch_address_space(hptproc);
     //printf("reset %d %d\n", p->p_nr, hptproc->p_nr);
     /* a enlever */
#endif

    if(p->p_hflags & FIRST_EXECUTION){
         /*The first eexecution just ended. Prepare to start the second
          * 
          */
         if(prepare_4_second_exec()==OK)
             return(RUN_SECOND);
         else
             return (E_RUN_SECOND);  
     }
     else{
         /*the second execution just ended. Prepare for comparison
          * if coomparison successed go to next processing element
          * else restart again the double execution plus comparison*/
          if(p->p_hflags & SECOND_EXECUTION){
              if(comparison_ws()==OK){
                  h_enable = 0;
                  h_proc_nr = 0;
                  //vm_setpt_root_to_ro(p, p->p_seg.p_cr3, 1);
                  vm_reset_pt(p,p->p_seg.p_cr3);
                  free_pram_mem_blocks(p);
                  return(END_HARD_PE);    
              }
              else{
                   if(prepare_4_first_exec()==OK)
                        return(RUN_FISRT);
                   else
                         return(E_RUN_FIRST);
              }
          }
          else{
               /* the hstate is neither FIRST_EXECUTION nor SECOND_EXECUTION
                * that should never happen*/
#if VERB
               printf("Une situation anormale vient de se produire\n");
#endif
               return (EBAD_STATE);
          }
     }

    
     return(OK);
}



