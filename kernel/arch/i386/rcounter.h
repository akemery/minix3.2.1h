#ifndef RCOUNTER_H
#define RCOUNTER_H

#include <minix/const.h>
#include <sys/cdefs.h>

#ifndef __ASSEMBLY__

#include <minix/com.h>
#include <minix/portio.h>

#define INS_2_EXEC 1200000
#define HARDENING_ZERO_STEP    0
#define HARDENING_FIRST_STEP   1
#define HARDENING_SECOND_STEP  2
#define INT_BY_SYSCALL         1
#define H_FORK_IN_PROGRESS   0x1
#define AT_WINI_PROC_NR    73132
#define H_START              135
#define H_ENABLE             0x2
#define H_SETRO              0x4
#define PMB_IS_USER          0x4
#define H_ENABLE1            0x8

#define WORKING_SET_SIZE    8192
#define PRAM_SLOT_FREE         1
#define PRAM_DOUBLE_FAULT      2
#define PT_MODIFIED            8
#define RUN_SECOND        (1<<1)
#define E_RUN_SECOND      (1<<2)
#define SECOND_EXECUTION  (1<<3)
#define END_HARD_PE       (1<<4)
#define RUN_FISRT         (1<<5)
#define E_RUN_FIRST       (1<<6)
#define EBAD_STATE        (1<<7)
#define FIRST_EXECUTION   (1<<8)
#define USE_INS_COUNTER        1
#define COW                    1


#define BEG_PRAM_MEM_BLOCK_ADDR (&working_set[0])
#define END_PRAM_MEM_BLOCK_ADDR (&working_set[WORKING_SET_SIZE -1])

#endif /* __ASSEMBLY__ */

#ifndef __ASSEMBLY__
/*table to store the modified process pages*/
struct pram_mem_block working_set[WORKING_SET_SIZE];


#endif /* __ASSEMBLY__ */

#endif /* RCOUNTER_H */

